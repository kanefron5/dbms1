#!/bin/bash

cd $ORACLE_HOME/dbs
orapwd FILE=orapwd$ORACLE_SID #qwerty

echo "
 db_name='coolsong'
 memory_target=1G
 sga_target=520M
 processes=150
 audit_trail ='db'
 db_block_size=4096
 db_domain=''
 db_recovery_file_dest='/u01/app/oracle/flash_recovery_area'
 db_recovery_file_dest_size=2G
 diagnostic_dest='/u01/app/oracle' dispatchers='(PROTOCOL=TCP)
 (SERVICE=ORCLXDB)'
 open_cursors=300
 remote_login_passwordfile='NONE'
 undo_tablespace='UNDOTBS1'
 control_files = (ora_control3, ora_control4)
 compatible ='11.2.0'
" > init$ORACLE_SID.ora

cd
mkdir -p $ORACLE_BASE/flash_recovery_area
mkdir -p /u01/vlk79
mkdir -p /u01/vlk79/logs
mkdir -p /u01/vlk79/coolsong/node01
mkdir -p /u01/vlk79/coolsong/node02
mkdir -p /u01/vlk79/coolsong/node03
mkdir -p /u01/vlk79/coolsong/node04

sqlplus /nolog @instance.sql
sqlplus /nolog @db_creator.sql
sqlplus /nolog @tablespace_creator.sql
sqlplus /nolog @dictionary_creator.sql