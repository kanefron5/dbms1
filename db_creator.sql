connect / as sysdba;
create database coolsong
    user sys identified by admin
    user system identified by admin
    logfile
        group 1 ('/u01/vlk79/logs/redo01a.log') size 50K,
        group 2 ('/u01/vlk79/logs/redo02a.log') size 50K
    maxloghistory 100
    maxlogfiles 5
    maxdatafiles 64
    character set UTF8
    national character set UTF8
    extent management local
    datafile
        '/u01/vlk79/coolsong/node01/ahuri80.dbf'
            size 50M
            reuse
            autoextend on
            maxsize unlimited,
        '/u01/vlk79/coolsong/node01/egoku38.dbf'
            size 50M
            reuse
            autoextend on
            maxsize unlimited,
        '/u01/vlk79/coolsong/node03/ojago6.dbf'
            size 50M
            reuse
            autoextend on
            maxsize unlimited
    sysaux datafile
        '/u01/vlk79/coolsong/node02/log10.dbf'
            size 50M
            reuse
            autoextend on
            maxsize unlimited,
        '/u01/vlk79/coolsong/node04/mor16.dbf'
            size 50M
            reuse
            autoextend on
            maxsize unlimited
    default tablespace users datafile
        '/u01/vlk79/coolsong/node02/udeqasi971.dbf'
            size 50M
            reuse
            autoextend on
            maxsize unlimited
    default temporary tablespace temp tempfile
        '/u01/vlk79/coolsong/temp01.dbf'
            size 50M
            reuse
            autoextend on
            maxsize unlimited
    undo tablespace undotbs1 datafile
        '/u01/vlk79/coolsong/undotbs01.dbf'
            size 50M
            reuse
            autoextend on
            maxsize unlimited;
