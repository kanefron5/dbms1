connect / as sysdba;
create tablespace LOUD_GRAY_MEAT 
    datafile
        '/u01/vlk79/coolsong/node04/loudgraymeat01.dbf'
            size 10M
            reuse
            autoextend on
            maxsize unlimited,
        '/u01/vlk79/coolsong/node01/loudgraymeat02.dbf' 
            size 10M
            reuse
            autoextend on
            maxsize unlimited,
        '/u01/vlk79/coolsong/node01/loudgraymeat03.dbf' 
            size 10M
            reuse
            autoextend on
            maxsize unlimited,
        '/u01/vlk79/coolsong/node04/loudgraymeat04.dbf' 
            size 10M
            reuse
            autoextend on
            maxsize unlimited,
        '/u01/vlk79/coolsong/node02/loudgraymeat05.dbf' 
            size 10M
            reuse
            autoextend on
            maxsize unlimited

create tablespace LOUD_GREEN_BIRD
    datafile
        '/u01/vlk79/coolsong/node03/loudgreenbird01.dbf'
            size 10M
            reuse
            autoextend on
            maxsize unlimited

create tablespace EASY_GREEN_DATA
    datafile
        '/u01/vlk79/coolsong/node04/easygreendata01.dbf'
            size 10M
            reuse
            autoextend on
            maxsize unlimited,
        '/u01/vlk79/coolsong/node01/easygreendata02.dbf'
            size 10M
            reuse
            autoextend on
            maxsize unlimited;
